apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: kibana
  namespace: logging
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: "Authentication Required"
spec:
  tls:
  - hosts:
    - kibana.{{ .Cluster.Domain }}
    secretName: {{ .Cluster.Name }}-tls
  rules:
  - host: kibana.{{ .Cluster.Domain }}
    http:
      paths:
      - path: /
        backend:
          serviceName: kibana
          servicePort: 5601
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: cerebro
  namespace: logging
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-realm: "Authentication Required"
spec:
  tls:
  - hosts:
    - cerebro.{{ .Cluster.Domain }}
    secretName: {{ .Cluster.Name }}-tls
  rules:
  - host: cerebro.{{ .Cluster.Domain }}
    http:
      paths:
      - path: /
        backend:
          serviceName: cerebro
          servicePort: 9000
---
apiVersion: v1
kind: Secret
metadata:
  name: basic-auth
  namespace: logging
type: Opaque
data:
  auth: {{ .Cluster.BasicAuth | b64encode }}
